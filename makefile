CXX=clang++
WEBCXX=em++
DROIDCXX=ndk-build
name=renderer
out_dir=rndr/bin
assets_dir=assets
SDL2_LIBS=`sdl2-config --cflags --libs`
RNDR_SRC=./rndr
DROID_PROJECT_DIR=./android_project

all:
	$(CXX) -c $(RNDR_SRC)/utils/loaders/lodepng/lodepng.cpp -g -o $(out_dir)/lodepng.o
	$(CXX) -std=c++11 test.cpp $(out_dir)/lodepng.o -g -Wall -I$(RNDR_SRC) -lGL -lGLEW $(SDL2_LIBS) -o $(out_dir)/$(name)
	
web:
	$(WEBCXX) -c $(RNDR_SRC)/utils/loaders/lodepng/lodepng.cpp -O3 -o $(out_dir)/lodepng.o
	$(WEBCXX) main.cpp $(out_dir)/lodepng.o -O2 $(SDL_LIBS) -o $(out_dir)/$(name).html -std=c++11 -I$(RNDR_SRC) -lGL -lGLEW -lSDL --preload-file ./$(assets_dir) -s TOTAL_MEMORY=16777216

android:
	CUR_DIR=`pwd`
	cd $(DROID_PROJECT_DIR)
	$(DROIDCXX)
	cd $(CUR_DIR)
	
clean:
	rm $(out_dir)/*