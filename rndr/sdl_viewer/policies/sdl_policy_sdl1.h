#include <stdexcept>
#include <string>
#include <gl/gl.hpp>
#include <emscripten.h>

#ifndef SDL_POLICY_SDL1_
#define SDL_POLICY_SDL1_

struct SDL1 {
    SDL1(const char* name, unsigned width, unsigned height, Uint32 flags = 0) {
        screen_ = SDL_SetVideoMode(width, height, 0, SDL_OPENGL | flags);
        if (screen_ == 0) {
            throw std::runtime_error(
                std::string("Failed to create window: ") + 
                std::string(SDL_GetError()));
        }
        SDL_WM_SetCaption(name, name);
        user_ctx_ = new gl::context();
    }

    ~SDL1() {
        delete user_ctx_;
        SDL_Quit();
    }

protected:
    void swap_() {
        SDL_GL_SwapBuffers();
    }

    bool quit_event_(const SDL_Event& event) const {
        return (event.type == SDL_QUIT);
    }

    bool resize_event_(const SDL_Event& event) const {
        return false;
    }

    void stop_() {
    }

    gl::context* user_ctx_;

private:
    SDL_Surface* screen_;
};

#endif /* SDL_POLICY_SDL1_ */
