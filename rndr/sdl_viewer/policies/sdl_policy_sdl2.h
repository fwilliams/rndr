#include <stdexcept>
#include <string>
#include <gl/gl.hpp>

#ifndef SDL_POLICY_SDL2_
#define SDL_POLICY_SDL2_

template<int MAJ, int MIN, bool IS_ES>
struct SDL2 {
    SDL2(const char* name, unsigned width, unsigned height, Uint32 flags = 0) {
        SDL_Init (SDL_INIT_EVERYTHING);
        if (IS_ES) {
            SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK,
                                SDL_GL_CONTEXT_PROFILE_ES);
        }

        if (MAJ > 0) {
            SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, MAJ);
            if (MIN >= 0) {
                SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, MIN);
            }
        }

        window_ = create_window(name, width, height, flags);
        sdl_ctx_ = SDL_GL_CreateContext(window_);
        user_ctx_ = new gl::context();
        running_ = true;
    }
    ~SDL2() {
        delete user_ctx_;
        SDL_GL_DeleteContext(sdl_ctx_);
        SDL_DestroyWindow(window_);
        SDL_Quit();
    }

    void delay(unsigned time) {
        SDL_Delay(time);
    }

    bool is_running() {
        return running_;
    }

protected:
    void swap_() {
        SDL_GL_SwapWindow(window_);
    }

    bool quit_event_(const SDL_Event& event) const {
        return (event.type == SDL_WINDOWEVENT &&
                event.window.event == SDL_WINDOWEVENT_CLOSE);
    }

    bool resize_event_(const SDL_Event& event) const {

    }

    void stop_() {
        running_ = false;
    }

    gl::context* user_ctx_;

private:
    SDL_Window* window_;
    SDL_GLContext sdl_ctx_;
    bool running_;

    static SDL_Window* create_window(const char* name, unsigned width,
                                     unsigned height, Uint32 flags = 0) {

        SDL_Init (SDL_INIT_EVERYTHING);

        SDL_Window* window_ = SDL_CreateWindow(name, SDL_WINDOWPOS_UNDEFINED,
                                               SDL_WINDOWPOS_UNDEFINED, width,
                                               height,
                                               SDL_WINDOW_OPENGL | flags);
        if (window_ == NULL) {
            throw std::runtime_error(
                std::string("Failed to create window: ") + 
                std::string(SDL_GetError()));
        }
        return window_;
    }
};

#endif /* SDL_POLICY_SDL2_ */
