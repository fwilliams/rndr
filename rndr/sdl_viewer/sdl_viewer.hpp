template<typename SDL_policy, typename viewer_policy>
struct sdl_viewer;

#define GL_MAJ_VERSION_AUTO  0
#define GL_MIN_VERSION_AUTO -1
#define GL_ES   true
#define GL_CORE false

#ifndef EMSCRIPTEN
#define sdl_viewer_main(scene, framerate, ...)         \
    int main(int argc, char** argv) {                   \
        viewer<scene> s(__VA_ARGS__);                   \
        s.begin();                                      \
        while(s.is_running()) {                         \
            s.main_loop_iter();                         \
            s.delay(1000/framerate);                    \
        }                                               \
        s.end();                                        \
    }

#else
#define sdl_viewer_main(scene, framerate, ...)         \
        static viewer<scene> s(__VA_ARGS__);                \
        void loopit_() { s.main_loop_iter(); }              \
        int main(int argc, char** argv) {                   \
            s.begin();                                      \
            emscripten_set_main_loop(loopit_, framerate, 1);\
            s.end();                                        \
        }
#endif

#ifdef EMSCRIPTEN
#include <SDL/SDL.h>
#include "policies/sdl_policy_sdl1.h"
template <typename VIEWER>
using viewer = sdl_viewer<SDL1, VIEWER>;
#elif defined(ANDROID)
#include <SDL.h>
#include "policies/sdl_policy_sdl2.h"
template <typename VIEWER>
using viewer = sdl_viewer<SDL2<2, GL_MIN_VERSION_AUTO, GL_ES>, VIEWER>;
#else
#include <SDL2/SDL.h>
#include "policies/sdl_policy_sdl2.h"
//#include "../../SDL/src/render/mmx.h"
template<typename VIEWER>
using viewer = sdl_viewer<SDL2<GL_MAJ_VERSION_AUTO, GL_MIN_VERSION_AUTO, GL_CORE>, VIEWER>;
#endif

#undef GL_MAJ_VERSION_AUTO
#undef GL_MIN_VERSION_AUTO
#undef GL_ES
#undef GL_CORE

#ifndef LIFECYCLE_H_
#define LIFECYCLE_H_

template<typename SDL_policy, typename viewer_policy>
struct sdl_viewer: public SDL_policy, public viewer_policy {
    sdl_viewer(const char* name, unsigned width, unsigned height, Uint32 flags =
                       0) :
            SDL_policy(name, width, height, flags), SDL_((SDL_policy&) (*this)), viewer_(
                    (viewer_policy&) (*this)) {
    }

    void begin() {
        this->init(*this->user_ctx_);
    }

    void main_loop_iter() {
        SDL_Event event;
        while (SDL_PollEvent(&event)) {
            if (this->quit_event_(event)) {
                this->stop_();
            }
            this->handle_event(*this->user_ctx_, event);
        }

        this->draw(*this->user_ctx_);
        this->swap_();
    }

    void end() {
        this->destroy(*this->user_ctx_);
    }

private:
    SDL_policy& SDL_;
    viewer_policy& viewer_;
};

#endif /* LIFECYCLE_H_ */
