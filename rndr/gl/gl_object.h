#ifndef GL_OBJECT_H_
#define GL_OBJECT_H_

namespace gl {
namespace detail {

struct gl_object {
    gl_object() :
            id_(0) {
    }

    bool is_null() {
        return id_ == 0;
    }

protected:
    gl_object(GLuint* id) :
            id_(id) {
    }

    GLuint* id_;
};

}
}
#endif /* GL_OBJECT_H_ */
