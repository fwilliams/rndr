#ifndef GL_CONTEXT_INIT_POLICY_NONE_H_
#define GL_CONTEXT_INIT_POLICY_NONE_H_

namespace gl {
namespace detail {

class gl_context_init_policy_none {
public:
    static void init_context() {
    }
};

}
}
#endif /* GL_CONTEXT_INIT_POLICY_NONE_H_ */
