#include <stdexcept>
#include <iostream>

#ifndef GL_CONTEXT_INIT_POLICY_GLEW_H_
#define GL_CONTEXT_INIT_POLICY_GLEW_H_

namespace gl {
namespace detail {

struct init_context_glew {
    void init_context() {
        glewExperimental = GL_TRUE;
        GLenum glew_init_res = glewInit();
        if (glew_init_res != GLEW_OK) {
            throw std::runtime_error(
                    std::string("GLEW Init error: ") +
                    std::string((const char*) glewGetErrorString(glew_init_res)));
        }
        std::clog << "OpenGL Context created: " << glGetString(GL_VERSION)
        << std::endl;
    }
};

}
}
#endif /* GL_CONTEXT_INIT_POLICY_GLEW_H_ */
