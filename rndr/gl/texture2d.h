#include <stdexcept>

#ifndef TEXTURE2D_H_
#define TEXTURE2D_H_

namespace gl {

namespace detail {

template<GLenum TARGET>
class texture2: public gl_object {
public:

    texture2() :
            gl_object() {
    }

    void bind() {
        glBindTexture(TARGET, *id_);
    }

    void unbind() {
        glBindTexture(TARGET, 0);
    }

    void set_data(GLint level, GLint internal_format, GLsizei width,
                  GLsizei height, GLenum format, GLenum type,
                  const GLvoid* data) {
        glTexImage2D(TARGET, level, internal_format, width, height, 0, format,
                     type, data);
    }

    void set_sub_data(GLint level, GLint xoffset, GLint yoffset, GLsizei width,
                      GLsizei height, GLenum format, GLenum type,
                      const GLvoid* data) {
        glTexSubImage2D(TARGET, level, xoffset, yoffset, width, height, format,
                        type, data);
    }

    void set_min_filter(GLint min_filter) {
        glTexParameteri(TARGET, GL_TEXTURE_MIN_FILTER, min_filter);
    }

    void set_mag_filter(GLint mag_filter) {
        glTexParameteri(TARGET, GL_TEXTURE_MAG_FILTER, mag_filter);
    }

    void set_wrap_s(GLint wrap_s) {
        glTexParameteri(TARGET, GL_TEXTURE_WRAP_S, wrap_s);
    }

    void set_wrap_t(GLint wrap_t) {
        glTexParameteri(TARGET, GL_TEXTURE_WRAP_T, wrap_t);
    }

    GLint get_min_filter() const {
        GLint res;
        glGetTexParameteriv(TARGET, GL_TEXTURE_MIN_FILTER, &res);
        return res;
    }

    GLint get_mag_filter() const {
        GLint res;
        glGetTexParameteriv(TARGET, GL_TEXTURE_MAG_FILTER, &res);
        return res;
    }

    GLint get_wrap_s() const {
        GLint res;
        glGetTexParameteriv(TARGET, GL_TEXTURE_WRAP_S, &res);
        return res;
    }

    GLint get_wrap_t() const {
        GLint res;
        glGetTexParameteriv(TARGET, GL_TEXTURE_WRAP_T, &res);
        return res;
    }

    bool is_null() const {
        return id_ == 0;
    }

private:
    friend context;

    texture2(GLuint* id) :
            gl_object(id) {
    }
};

}

using texture2d = detail::texture2<GL_TEXTURE_2D>;

}

#endif /* TEXTURE2D_H_ */
