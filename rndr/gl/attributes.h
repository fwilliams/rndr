#include <vector>
#include <utils/types/types.hpp>

#ifndef ATTRIBUTES_H_
#define ATTRIBUTES_H_

namespace gl {

    struct attribute {
        GLuint index;
        GLint size;
        GLenum type;
        size_t offset;
    };

    template <typename T>
    struct attribute_batch {
    public:
        attribute_batch(const program& program) {
            const member_t* members = T::info().members();
            GLuint attrib_loc;
            GLint max_attribs = program.get_max_vertex_attribs();

            for(int i = 0; i < T::info().member_count(); ++i) {
                attrib_loc = program.get_attribute_location(members[i].name);
                if(attrib_loc < max_attribs) {
                    attribs_.push_back( {
                                attrib_loc,
                                (GLint) members[i].num_elems,
                                (GLenum) members[i].enum_type,
                                (size_t) members[i].offset
                            });
                }
            }
        }

        std::vector<attribute>& get_attributes() {
            return attribs_;
        }

        void sync(vbo<T> vbo_) {
            for(auto i = attribs_.begin(); i != attribs_.end(); ++i) {
                vbo_.enable_vertex_attrib_array(i->index);
                vbo_.set_attrib_pointer(i->index, i->size, i->type, i->offset, GL_FALSE);
            }
        }

    private:
        std::vector<attribute> attribs_;
    };

}
#endif /* ATTRIBUTES_H_ */
