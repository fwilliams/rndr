#include <vector>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>

#ifndef PROGRAM_H_
#define PROGRAM_H_

namespace gl {

template<typename T>
struct uniform_batch;

template<typename T>
struct attribute_batch;

namespace detail {

class program: public gl_object {
public:

    program() :
            gl_object() {
    }

    template<typename V>
    attribute_batch<V>& get_attributes() const {
        static attribute_batch<V> batch(*this);
        return batch;
    }

    template<typename T>
    void set_uniforms_from_struct(T& uniforms) {
        static uniform_batch<T> batch(*this);
        batch.sync_with_offset(&uniforms);
    }

    void set_uniform(GLuint location, GLfloat value, const GLsizei count = 1,
                     const GLboolean transpose = GL_FALSE) {
        glProgramUniform1fEXT(*id_, location, value);
    }

    void set_uniform(GLuint location, GLint value, const GLsizei count = 1,
                     const GLboolean transpose = GL_FALSE) {
        glProgramUniform1iEXT(*id_, location, value);
    }

    void set_uniform(GLuint location, const glm::vec2& value,
                     const GLsizei count = 1, const GLboolean transpose =
                             GL_FALSE) {
        glProgramUniform2fEXT(*id_, location, value.x, value.y);
    }

    void set_uniform(GLuint location, const glm::vec3& value,
                     const GLsizei count = 1, const GLboolean transpose =
                             GL_FALSE) {
        glProgramUniform3fEXT(*id_, location, value.x, value.y, value.z);
    }

    void set_uniform(GLuint location, const glm::vec4& value,
                     const GLsizei count = 1, const GLboolean transpose =
                             GL_FALSE) {
        glProgramUniform4fEXT(*id_, location, value.x, value.y, value.z,
                              value.w);
    }

    void set_uniform(GLuint location, const glm::mat2& value,
                     const GLsizei count = 1, const GLboolean transpose =
                             GL_FALSE) {
        glProgramUniformMatrix2fv(*id_, location, count, transpose,
                                  glm::value_ptr(value));
    }

    void set_uniform(GLuint location, const glm::mat3& value,
                     const GLsizei count = 1, const GLboolean transpose =
                             GL_FALSE) {
        glProgramUniformMatrix3fv(*id_, location, count, transpose,
                                  glm::value_ptr(value));
    }

    void set_uniform(GLuint location, const glm::mat4& value,
                     const GLsizei count = 1, const GLboolean transpose =
                             GL_FALSE) {
        glProgramUniformMatrix4fv(*id_, location, count, transpose,
                                  glm::value_ptr(value));
    }

    void set_uniform(GLuint location, texture2d& value, GLuint texunit) {
        glActiveTexture(GL_TEXTURE0 + texunit);
        value.bind();
        set_uniform(location, (GLint) texunit);
    }

    GLint get_uniform_location(const GLchar* name) const {
        return glGetUniformLocation(*id_, name);
    }

    GLuint get_attribute_location(const GLchar* name) const {
        return glGetAttribLocation(*id_, name);
    }

    GLint get_max_vertex_attribs() const {
        GLint max_attribs;
        glGetIntegerv(GL_MAX_VERTEX_ATTRIBS, &max_attribs);
        return max_attribs;
    }

    bool operator==(const program& other) const {
        return id_ == other.id_;
    }

    bool operator!=(const program& other) const {
        return id_ != other.id_;
    }

private:
    friend context;

    void bind() {
        glUseProgram(*id_);
    }

    program(GLuint* id) :
            gl_object(id) {
    }
};

}

using program = detail::program;

}
#endif /* PROGRAM_H_ */
