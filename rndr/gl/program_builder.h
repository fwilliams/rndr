#include <errno.h>
#include <fstream>
#include <sstream>
#include <utils/log/log.hpp>
#include <utils/exceptions/exceptions.hpp>

#ifndef PROGRAM_BUILDER_H_
#define PROGRAM_BUILDER_H_

namespace gl {
namespace detail {

struct program_builder {
public:
    static GLuint build_from_files(const std::string& vert,
                                   const std::string& frag) {
        GLuint program = glCreateProgram();

        GLuint vert_shader = compile_from_file(GL_VERTEX_SHADER, vert);
        GLuint frag_shader = compile_from_file(GL_FRAGMENT_SHADER, frag);

        glAttachShader(program, vert_shader);
        glAttachShader(program, frag_shader);
        glLinkProgram(program);

        if (!log_link_status(program))
            exceptions::runtime_error("Failed to link shaders");
        glDeleteShader(vert_shader);
        glDeleteShader(frag_shader);

        return program;
    }

    static GLuint build_from_strings(const std::string& vert,
                                     const std::string& frag) {
        GLuint program = glCreateProgram();
        GLuint vert_shader = compile(GL_VERTEX_SHADER, vert);
        GLuint frag_shader = compile(GL_FRAGMENT_SHADER, frag);
        glAttachShader(program, vert_shader);
        glAttachShader(program, frag_shader);
        glLinkProgram(program);

        log_link_status(program);
        glDeleteShader(vert_shader);
        glDeleteShader(frag_shader);
        return program;
    }

private:
    static std::string read_file_to_string(const std::string& filename) {
        std::ifstream in(filename.c_str(), std::ios::in | std::ios::binary);
        if (in) {
            std::string contents;
            in.seekg(0, std::ios::end);
            contents.resize((unsigned int) in.tellg());
            in.seekg(0, std::ios::beg);
            in.read(&contents[0], contents.size());
            in.close();
            return (contents);
        }
        std::ostringstream err;
        err << "Error reading file " << filename << " with ERRNO: "
            << strerror(errno);
        exceptions::runtime_error(err.str().c_str());
        return "";
    }

    static GLuint compile(const GLenum type, const std::string& src) {
        const GLchar* source = src.c_str();
        GLint length = src.length();

        GLuint shader = glCreateShader(type);
        glShaderSource(shader, 1, &source, &length);

        logger::log_info("Compiling %s...\n",
                         shader_type_as_string(type).c_str());
        glCompileShader(shader);

        if (log_compile_status(shader, type) != 0) {
            exceptions::runtime_error("Compilation failed\n");
        }

        return shader;
    }

    static GLuint compile_from_file(const GLenum type,
                                    const std::string& file_path) {
        logger::log_info("Reading %s: %s\n",
                         shader_type_as_string(type).c_str(),
                         file_path.c_str());

        std::string src = read_file_to_string(file_path);
        return compile(type, src);
    }

    static GLint log_compile_status(const GLuint shader_id, const GLenum type) {
        GLint success = GL_FALSE;
        glGetShaderiv(shader_id, GL_COMPILE_STATUS, &success);
        std::string log = get_compile_log(shader_id);
        if (success != GL_TRUE) {
            logger::log_error("Error compiling %s:\n\n",
                              shader_type_as_string(type).c_str());
            logger::log_error("Compile log:\n");
            logger::log_error("%s\n", log.c_str());

            return -1;
        } else {
            logger::log_info("Successfully compiled %s:\n\n",
                             shader_type_as_string(type).c_str());
            if (log.length() > 0) {
                logger::log_info("Compile log:\n");
                logger::log_info("%s\n", log.c_str());
            }
            return 0;
        }
    }

    static std::string get_compile_log(const GLuint shader_id) {
        GLint length;
        glGetShaderiv(shader_id, GL_INFO_LOG_LENGTH, &length);
        std::string log(length, ' ');
        glGetShaderInfoLog(shader_id, length, &length, &log[0]);
        return log;
    }

    static std::string shader_type_as_string(const GLenum shader_type) {
        switch (shader_type) {
        case GL_VERTEX_SHADER:
            return std::string("vertex shader");
            break;
        case GL_FRAGMENT_SHADER:
            return std::string("fragment shader");
            break;
        default:
            return std::string("");
        }
    }

    static bool log_link_status(GLuint program_id) {
        GLint success;
        glGetProgramiv(program_id, GL_LINK_STATUS, &success);
        if (success == GL_FALSE) {
            GLint info_log_length;
            glGetProgramiv(program_id, GL_INFO_LOG_LENGTH, &info_log_length);

            GLchar str_info_log[info_log_length + 1];
            glGetProgramInfoLog(program_id, info_log_length, 0, str_info_log);
            logger::log_error("Error linking shader:\n");
            logger::log_error("%s\n", str_info_log);
            return false;
        } else {
            logger::log_info("Successfully linked program\n");
            return true;
        }
    }
};

}
}
#endif /* PROGRAM_BUILDER_H_ */
