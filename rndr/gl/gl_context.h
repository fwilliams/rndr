#include <list>

#ifndef GL_CONTEXT_H_
#define GL_CONTEXT_H_

namespace gl {
namespace detail {

template<class gl_context_init_policy>
class gl_context: gl_context_init_policy {
public:
    gl_context() :
            context_init_policy((gl_context_init_policy&) (*this)) {
        context_init_policy.init_context();
    }

    void bind_program(program& prog) {
        prog.bind();
    }

    template<typename T>
    void bind_ibo(ibo<T>& ibo) {
        ibo.bind();
    }

    template<typename T>
    void bind_vbo(vbo<T>& vbo) {
        vbo.bind();
    }

    void unbind_program() {
        glUseProgram(0);
    }

    void unbind_vbo() {
        glBindBuffer(GL_ARRAY_BUFFER, 0);
    }

    void unbind_ibo() {
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    }

    template<typename T>
    vbo<T> create_vbo() {
        GLuint* id = this->allocate_gl_object_();
        glGenBuffers(1, id);
        return vbo<T>(id);
    }

    template<typename T>
    ibo<T> create_ibo() {
        GLuint* id = this->allocate_gl_object_();
        glGenBuffers(1, id);
        return ibo<T>(id);
    }

    program create_program_from_files(const std::string& vert,
                                      const std::string& frag) {
        GLuint* id = this->allocate_gl_object_();
        *id = detail::program_builder::build_from_files(vert, frag);
        return program(id);
    }

    program create_program_from_strings(const std::string& vert,
                                        const std::string& frag) {
        GLuint* id = this->allocate_gl_object_();
        *id = detail::program_builder::build_from_strings(vert, frag);
        return program(id);
    }

    texture2d create_texture2d() {
        GLuint* id = this->allocate_gl_object_();
        glGenTextures(1, id);
        return texture2d(id);
    }

    void enable_depth_test() {
        glEnable(GL_DEPTH_TEST);
    }

    void disable_depth_test() {
        glDisable(GL_DEPTH_TEST);
    }

    void set_clear_color(glm::vec4 color) {
        glClearColor(color.r, color.g, color.b, color.a);
    }

    void set_clear_color(GLclampf r, GLclampf g, GLclampf b, GLclampf a) {
        glClearColor(r, g, b, a);
    }

    void clear(GLbitfield mask) {
        glClear(mask);
    }

    void set_viewport(GLint x, GLint y, GLsizei width, GLsizei height) {
        glViewport(x, y, width, height);
    }

private:
    gl_context(gl_context<gl_context_init_policy> const&);
    void operator=(gl_context<gl_context_init_policy> const&);

    inline GLuint* allocate_gl_object_() {
        allocated_objects_.push_back(0);
        return &allocated_objects_.back();
    }

    gl_context_init_policy& context_init_policy;

    std::list<GLuint> allocated_objects_;
};

}
}
#endif /* GL_CONTEXT_H_ */
