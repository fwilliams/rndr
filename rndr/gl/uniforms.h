#include <utils/types/types.hpp>
#include <sstream>

#ifndef UNIFORMS_H_
#define UNIFORMS_H_

namespace gl {

    class abstract_uniform {
    public:
        virtual void sync() = 0;
        virtual void sync_with_offset(const GLvoid* base_ptr) = 0;

        virtual ~abstract_uniform() {
        }

        static std::vector<abstract_uniform*> generate_uniforms(program& program,
                const reflection_base& info, const GLvoid* data_ptr = 0, const char* prefix = "");

        static abstract_uniform* create_uniform_for_type(program& program,
                GLuint loc, GLint typeconstant, GLuint& tex_unit, const GLvoid* data_ptr = 0, GLuint array_index = 0);

    };

    template<typename T>
    class uniform: public abstract_uniform {
    public:
        uniform(program& program, GLuint loc, GLuint texunit = 0, T* data_ptr = 0) :
        program_(program), loc_(loc), data_ptr_(data_ptr) {
        }

        void sync() {
            program_.set_uniform(loc_, *data_ptr_);
        }

        void sync_with_offset(const GLvoid* base_ptr) {
            program_.set_uniform(loc_, *((T*) (((GLubyte*) data_ptr_) + ((size_t)(base_ptr)))));
        }

        T* get_data_pointer() const {
            return data_ptr_;
        }

        GLint get_location() const {
            return loc_;
        }

        const program& get_program() const {
            return program_;
        }

    private:
        program program_;
        GLint loc_;
        T* data_ptr_;
    };

    template<>
    class uniform<texture2d> : public abstract_uniform {
    public:
        uniform(program& program, GLuint loc, GLuint texunit = 0,
                texture2d* data_ptr = 0) :
        program_(program), loc_(loc), data_ptr_(data_ptr), tex_unit_(
                texunit) {
        }

        void sync() {
            program_.set_uniform(loc_, *data_ptr_, tex_unit_);
        }

        void sync_with_offset(const GLvoid* base_ptr) {
            texture2d* tex = ((texture2d*) (((GLubyte*) data_ptr_) + ((size_t)(base_ptr))));
            if(!tex->is_null()) {
                program_.set_uniform(loc_, *tex, tex_unit_);
            }
        }

    private:
        program program_;
        GLint loc_;
        texture2d* data_ptr_;
        GLuint tex_unit_;
    };

    template<typename T>
    struct uniform_batch {
        uniform_batch(program& program) :
        program_(program) {
            uniforms_ = abstract_uniform::generate_uniforms(program_, T::info(), 0);
        }

        uniform_batch(program& program, T* base) :
        program_(program) {
            uniforms_ = abstract_uniform::generate_uniforms(program_, T::info(), base);
        }

        virtual ~uniform_batch() {
            // FIXME: Deleting uniform batch deletes underlying data for any copies
            // We don't run into this problem currently because the framework only
            // creates static uniform batches. If we do want to copy them around,
            // this needs to be done
            for (int i = 0; i < uniforms_.size(); i++) {
                delete uniforms_[i];
            }
        }

        void sync() {
            for (int i = 0; i < uniforms_.size(); i++) {
                uniforms_[i]->sync();
            }
        }

        void sync_with_offset(const GLvoid* base_ptr) {
            for (int i = 0; i < uniforms_.size(); i++) {
                uniforms_[i]->sync_with_offset(base_ptr);
            }
        }

    private:
        program program_;
        std::vector<abstract_uniform*> uniforms_;
    };

    abstract_uniform* abstract_uniform::create_uniform_for_type(program& program,
            GLuint loc, GLint typeconstant, GLuint& tex_unit, const GLvoid* data_ptr, GLuint array_index) {
        switch (typeconstant) {
            case GL_TEXTURE_2D:
            return new uniform<texture2d>(program, loc, tex_unit++,
                    (texture2d*) data_ptr + array_index*sizeof(texture2d));
            case GL_INT:
            return new uniform<GLint>(program, loc, tex_unit, (GLint*) (((GLubyte*)data_ptr) + array_index*sizeof(GLint)));
            case GL_FLOAT:
            return new uniform<GLfloat>(program, loc, tex_unit, (GLfloat*) (((GLubyte*)data_ptr) + array_index*sizeof(GLfloat)));
            case GL_FLOAT_VEC2:
            return new uniform<glm::vec2>(program, loc, tex_unit,
                    (glm::vec2*) (((GLubyte*)data_ptr) + array_index*sizeof(glm::vec2)));
            case GL_FLOAT_VEC3:
            return new uniform<glm::vec3>(program, loc, tex_unit,
                    (glm::vec3*) (((GLubyte*)data_ptr) + array_index*sizeof(glm::vec3)));
            case GL_FLOAT_VEC4:
            return new uniform<glm::vec4>(program, loc, tex_unit,
                    (glm::vec4*) (((GLubyte*)data_ptr) + array_index*sizeof(glm::vec4)));
            case GL_FLOAT_MAT2:
            return new uniform<glm::mat2>(program, loc, tex_unit,
                    (glm::mat2*) (((GLubyte*)data_ptr) + array_index*sizeof(glm::mat2)));
            case GL_FLOAT_MAT3:
            return new uniform<glm::mat3>(program, loc, tex_unit,
                    (glm::mat3*) (((GLubyte*)data_ptr) + array_index*sizeof(glm::mat3)));
            case GL_FLOAT_MAT4:
            return new uniform<glm::mat4>(program, loc, tex_unit,
                    (glm::mat4*) (((GLubyte*)data_ptr) + array_index*sizeof(glm::mat4)));
            default:
            return 0; // TODO: Handle this case
        }
    }

    std::vector<abstract_uniform*> abstract_uniform::generate_uniforms(
            program& program, const reflection_base& info, const GLvoid* data_ptr,
            const char* prefix) {
        GLint loc = 0;
        GLuint texunit = 0;
        const member_t* members = info.members();
        std::vector<abstract_uniform*> uniforms;
        std::stringstream ss;
        for (int i = 0; i < info.member_count(); i++) {
            ss.str("");
            if(members[i].is_array && members[i].is_struct) { // Array of structs
                for(int j = 0; j < members[i].array_size; j++) {
                    ss.str("");
                    ss << prefix << members[i].name << "[" << j << "].";
                    const GLvoid* ptr = (const GLvoid*)
                    (((GLbyte*)(data_ptr)) +
                            members[i].offset +
                            members[i].refl_base->size()*j);

                    std::vector<abstract_uniform*> obj_unifs =
                    generate_uniforms(program, *members[i].refl_base,
                            ptr, ss.str().c_str());
                    uniforms.insert(uniforms.end(), obj_unifs.begin(), obj_unifs.end());
                }

            } else if(members[i].is_array && !members[i].is_struct) { // Array of POD
                for(int j = 0; j < members[i].array_size; j++) {
                    ss.str("");
                    ss << prefix << members[i].name << "[" << j << "]";
                    loc = program.get_uniform_location(ss.str().c_str());
                    if (loc >= 0) {
                        abstract_uniform* uniform = create_uniform_for_type(program, loc,
                                members[i].gl_type_constant, texunit,
                                (const GLvoid*) (((GLbyte*)(data_ptr)) + members[i].offset), j);
                        uniforms.push_back(uniform);
                    }
                }

            } else if(!members[i].is_array && members[i].is_struct) { // Struct
                ss << prefix << members[i].name << ".";
                std::vector<abstract_uniform*> obj_unifs =
                generate_uniforms(program, *members[i].refl_base,
                        (const GLvoid*) (((GLbyte*)(data_ptr)) + members[i].offset), ss.str().c_str());
                uniforms.insert(uniforms.end(), obj_unifs.begin(), obj_unifs.end());

            } else { // POD
                ss << prefix << members[i].name;
                loc = program.get_uniform_location(ss.str().c_str());
                if (loc >= 0) {
                    abstract_uniform* uniform = create_uniform_for_type(program, loc,
                            members[i].gl_type_constant, texunit,
                            (const GLvoid*) (((GLbyte*)(data_ptr)) + members[i].offset));
                    uniforms.push_back(uniform);
                }
            }

        }
        return uniforms;
    }

}
#endif /* UNIFORMS_H_ */
