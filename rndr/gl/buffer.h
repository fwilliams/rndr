#include <typeinfo>

#ifndef BUFFER_H_
#define BUFFER_H_

namespace gl {
namespace detail {

template<GLenum TARGET, typename T>
class buffer: public gl_object {
public:
    buffer() :
            gl_object() {
    }

    void set_data(GLsizei num_elements, const T* data, GLenum usage) {
        glNamedBufferDataEXT(*id_, (GLsizeiptr) (num_elements * sizeof(T)),
                             (const GLvoid*) data, usage);
    }

    void set_sub_data(GLint element_offset, GLsizei num_elements,
                      const T* data) {
        glNamedBufferSubDataEXT(*id_, (GLintptr) (element_offset * sizeof(T)),
                                (GLsizeiptr) (num_elements * sizeof(T)),
                                (const GLvoid*) data);
    }

    void setup_attrib_array(attribute_batch<T>& attribs) {
        for (auto i = attribs.get_attributes().begin();
                i != attribs.get_attributes().end(); ++i) {
            enable_vertex_attrib_array(i->index);
            set_attrib_pointer(i->index, i->size, i->type, i->offset, GL_FALSE);
        }
    }

    void set_attrib_pointer(GLuint index, GLint size, GLenum type,
                            size_t offset, GLboolean normalized) {
        glVertexAttribPointer(index, size, type, normalized, sizeof(T),
                              (const GLvoid*) offset);
    }

    void enable_vertex_attrib_array(GLuint index) {
        glEnableVertexAttribArray(index);
    }

    void disable_vertex_attrib_array(GLuint index) {
        glDisableVertexAttribArray(index);
    }

    size_t get_byte_count() const {
        GLint res;
        glGetNamedBufferParameterivEXT(*id_, GL_BUFFER_SIZE, &res);
        return res;
    }

    size_t get_element_count() const {
        GLint res;
        glGetNamedBufferParameterivEXT(*id_, GL_BUFFER_SIZE, &res);
        return res / sizeof(T);
    }

    GLenum get_usage() const {
        GLint res;
        glGetNamedBufferParameterivEXT(*id_, GL_BUFFER_USAGE, &res);
        return res;
    }

private:
    friend context;

    void bind() {
        glBindBuffer(TARGET, *id_);
    }

    buffer(GLuint* id) :
            gl_object(id) {
    }
};

}

template<typename T>
using vbo = detail::buffer<GL_ARRAY_BUFFER, T>;

template<typename T>
using ibo = detail::buffer<GL_ELEMENT_ARRAY_BUFFER, T>;

}
#endif /* BUFFER_H_ */
