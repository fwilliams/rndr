#ifndef GL_HPP_
#define GL_HPP_

#if defined(EMSCRIPTEN)
#include <GL/glew.h>
#include "policies/gl_context_init_policy_none.h"

namespace gl {
    namespace detail {
        template<class gl_context_policy>
        class gl_context;
    }
    typedef detail::gl_context<detail::gl_context_init_policy_none> context;
}
#elif defined(ANDROID)
#include <GLES2/gl2.h>
#include "policies/gl_context_init_policy_none.h"

namespace gl {
    namespace detail {
        template<class gl_context_policy>
        class gl_context;
    }
    typedef detail::gl_context<detail::gl_context_init_policy_none> context;
}
#else
#include <GL/glew.h>
#include "policies/gl_context_init_policy_glew.h"

namespace gl {
namespace detail {
template<class gl_context_policy>
class gl_context;
}
typedef detail::gl_context<detail::init_context_glew> context;
}
#endif

#include "gl_object.h"
#include "texture2d.h"
#include "program.h"
#include "buffer.h"
#include "attributes.h"
#include "uniforms.h"
#include "program_builder.h"
#include "gl_context.h"
#include "attributes.h"

#endif /* GL_HPP_ */
