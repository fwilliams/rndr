#ifndef EXCEPTIONS_HPP_
#define EXCEPTIONS_HPP_

namespace exceptions {

#ifdef NO_EXCEPTIONS
#include "no_exceptions.h"
#else
#include "std_exceptions.h"
#endif

}
#endif /* EXCEPTIONS_HPP_ */
