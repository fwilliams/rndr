#include <stdexcept>

#ifndef STD_EXCEPTIONS_H_
#define STD_EXCEPTIONS_H_

void runtime_error(const char* error_str) {
    throw std::runtime_error(error_str);
}

#endif /* STD_EXCEPTIONS_H_ */
