#include <cstdio>
#include <cstdarg>
#include <string>

#ifndef PRINTF_LOG_H_
#define PRINTF_LOG_H_

void log_error(const char* format_str, ...) {
    va_list args;
    va_start(args, format_str);
    vfprintf(stderr, format_str, args);
    va_end(args);
}

void log_info(const char* format_str, ...) {
    va_list args;
    va_start(args, format_str);
    vfprintf(stdout, format_str, args);
    va_end(args);
}

#endif /* PRINTF_LOG_H_ */
