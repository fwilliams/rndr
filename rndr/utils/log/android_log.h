#include <android/log.h>
#include <cstdarg>

#ifndef ANDROID_LOG_H_
#define ANDROID_LOG_H_

#define __LOG_TAG__ "GL framework"

void log_error(const char* format_str, ...) {
    va_list args;
    va_start(args, format_str);
    __android_log_print(ANDROID_LOG_ERROR, __LOG_TAG__, format_str, args);
    va_end(args);
}

void log_info(const char* format_str, ...) {
    va_list args;
    va_start(args, format_str);
    __android_log_print(ANDROID_LOG_INFO, __LOG_TAG__, format_str, args);
    va_end(args);
}

#undef __LOG_TAG__

#endif /* ANDROID_LOG_H_ */
