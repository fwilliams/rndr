#ifndef LOG_HPP_
#define LOG_HPP_

namespace logger {

#if defined(DISABLE_LOG)
#include "no_log.h"
#elif defined(ANDROID)
#include "android_log.h"
#else
#include "printf_log.h"
#endif

}
#endif /* LOG_HPP_ */
