#include <vector>
#include <string>
#include <sstream>
#include <algorithm>
#include <iterator>

#ifndef PARSING_HPP_
#define PARSING_HPP_

// TODO: Move face errors to OBJ loader
enum parse_error {
    NO_ERROR,
    TOO_FEW_TOKENS,
    TOO_MANY_TOKENS,
    TOO_MANY_FACE_INDICES,
    MALFORMED_FACE
};

/*
 * Split a string into tokens by whitespace
 */
std::vector<std::string> tokenize(const std::string& line) {
    using namespace std;
    vector<string> tokens;
    istringstream ss(line);
    copy(istream_iterator<string>(ss), istream_iterator<string>(),
         back_inserter(tokens));

    return tokens;
}

/*
 * Split a string into tokens by a delimeter string
 */
std::vector<std::string> tokenize(const std::string& line,
                                  const std::string& delim) {
    using namespace std;
    vector<string> tokens;
    string line_copy = line;
    size_t pos = 0;
    while ((pos = line_copy.find(delim)) != string::npos) {
        tokens.push_back(line_copy.substr(0, pos));
        line_copy.erase(0, pos + delim.length());
    }
    tokens.push_back(line_copy);
    return tokens;
}

template<typename CONTAINER>
CONTAINER parse_tokens_to_container(const std::vector<std::string>& tokens,
                                    const CONTAINER& default_value,
                                    size_t required_fields,
                                    parse_error& out_error) {

    using namespace std;
    CONTAINER ret = default_value;
    unsigned char index = 0;
    for (auto i = tokens.begin(); i != tokens.end(); i++) {
        istringstream vec_ss(*i);
        vec_ss >> ret[index++];
    }
    if (tokens.size() > required_fields) {
        out_error = TOO_MANY_TOKENS;
    } else if (tokens.size() < required_fields) {
        out_error = TOO_FEW_TOKENS;
    } else {
        out_error = NO_ERROR;
    }

    return ret;
}

#endif /* PARSING_HPP_ */
