#include <iostream>
#include <string>
#include <fstream>
#include <iterator>
#include <vector>
#include <map>

#include <utils/types/types.hpp>
#include <utils/parsing/parsing.hpp>
#include <utils/loaders/lodepng/lodepng.h>
#include <utils/log/log.hpp>

template<typename VERTEX, typename MATERIAL>
class obj_model {
public:
    obj_model(gl::context& ctx, const std::string& filename,
              const std::string& base_dir = ".") :
            ctx_(ctx), base_dir_(base_dir) {
        load_obj_from_file(filename);
    }

    std::vector<MATERIAL*> mats;
    std::vector<gl::vbo<VERTEX>> geometry;
    std::map<std::string, MATERIAL> material_map;

private:
    void load_obj_from_file(const std::string& filename) {
        using namespace std;
        using namespace glm;

        ifstream in(base_dir_ + string("/") + filename);
        string line;
        vector<string> line_tokens;

        vector<vec4> positions;
        vector<vec3> normals;
        vector<vec3> texcoords;

        map<string, vector<VERTEX>> vertex_data;

        vector<VERTEX>* current_batch;

        while (getline(in, line)) {
            line_tokens = tokenize(line);

            if (line_tokens.size() > 0) {
                parse_error err;
                if (line_tokens[0].compare("v") == 0) {
                    vector<string> data_tokens(line_tokens.begin() + 1,
                                               line_tokens.end());
                    positions.push_back(
                            parse_tokens_to_container(data_tokens,
                                                      vec4(0, 0, 0, 1), 3,
                                                      err));

                } else if (line_tokens[0].compare("vt") == 0) {
                    vector<string> data_tokens(line_tokens.begin() + 1,
                                               line_tokens.end());
                    texcoords.push_back(
                            parse_tokens_to_container(data_tokens,
                                                      vec3(0, 0, 1), 2, err));

                } else if (line_tokens[0].compare("vn") == 0) {
                    vector<string> data_tokens(line_tokens.begin() + 1,
                                               line_tokens.end());
                    normals.push_back(
                            normalize(
                                    parse_tokens_to_container(data_tokens,
                                                              vec3(0, 0, 0), 3,
                                                              err)));

                } else if (line_tokens[0].compare("vp") == 0) {
                    // Currently unused

                } else if (line_tokens[0].compare("f") == 0) {
                    vector<string> data_tokens(line_tokens.begin() + 1,
                                               line_tokens.end());

                    VERTEX v = parse_face(*data_tokens.begin(), positions,
                                          texcoords, normals, err);
                    for (auto i = data_tokens.begin();
                            i != data_tokens.end() - 2; ++i) {
                        current_batch->push_back(v);
                        current_batch->push_back(
                                parse_face(*(i + 1), positions, texcoords,
                                           normals, err));
                        current_batch->push_back(
                                parse_face(*(i + 2), positions, texcoords,
                                           normals, err));
                    }

                } else if (line_tokens[0].compare("mtllib") == 0) {
                    load_mtl_from_file(
                            base_dir_ + string("/") + line_tokens[1]);

                } else if (line_tokens[0].compare("usemtl") == 0) {
                    current_batch = &vertex_data[line_tokens[1]];

                } else if (line_tokens[0][0] == '#') {

                } else {
                    throw logic_error("Invalid OBJ syntax");
                }
            }
        }
        for (auto i = vertex_data.begin(); i != vertex_data.end(); ++i) {
            gl::vbo<VERTEX> v = ctx_.create_vbo<VERTEX>();
            v.bind();
            v.set_data(i->second.size(), &(i->second[0]), GL_STATIC_DRAW);
            v.unbind();
            geometry.push_back(v);
            mats.push_back(&(material_map.at(i->first)));
        }
    }

    void load_mtl_from_file(const std::string& filename) {
        using namespace std;
        using namespace glm;

        ifstream in(filename);
        string line;

        vector<string> line_tokens;
        MATERIAL* current_material;
        map<string, gl::texture2d> textures;

        while (getline(in, line)) {
            parse_error err;
            line_tokens = tokenize(line);
            if (line_tokens.size() > 0) {
                if (line_tokens[0].compare("newmtl") == 0) {
                    current_material = &material_map[line_tokens[1]];

                } else if (line_tokens[0].compare("Ka") == 0) {
                    vector<string> data_tokens(line_tokens.begin() + 1,
                                               line_tokens.end());
                    current_material->ambient = parse_tokens_to_container(
                            data_tokens, vec4(0), 4, err);

                } else if (line_tokens[0].compare("Kd") == 0) {
                    vector<string> data_tokens(line_tokens.begin() + 1,
                                               line_tokens.end());
                    current_material->diffuse = parse_tokens_to_container(
                            data_tokens, vec4(0), 4, err);

                } else if (line_tokens[0].compare("Ks") == 0) {
                    vector<string> data_tokens(line_tokens.begin() + 1,
                                               line_tokens.end());
                    current_material->specular = parse_tokens_to_container(
                            data_tokens, vec4(0), 4, err);
                } else if (line_tokens[0].compare("d") == 0
                        || line_tokens[0].compare("Tr") == 0) {
                    // Currently unused

                } else if (line_tokens[0].compare("Ns") == 0) {
                    vector<string> data_tokens(line_tokens.begin() + 1,
                                               line_tokens.end());
                    current_material->shine_exp = 1.0;
                }
                if (line_tokens[0].compare("illum") == 0) {
                    // Currently using default illumination model

                } else if (line_tokens[0].compare("map_Ka") == 0) {
                    string path = base_dir_ + string("/") + line_tokens[1];
                    if (textures.find(path) == textures.end()) {
                        textures[path] = load_texture(ctx_, path);
                    }
                    current_material->amb_tex = textures[path];

                } else if (line_tokens[0].compare("map_Kd") == 0) {
                    // TODO: Non hardcoded texture parameters
                    string path = base_dir_ + string("/") + line_tokens[1];
                    if (textures.find(path) == textures.end()) {
                        textures[path] = load_texture(ctx_, path);
                    }
                    current_material->dif_tex = textures[path];

                } else if (line_tokens[0].compare("map_Ks") == 0) {
                    string path = base_dir_ + string("/") + line_tokens[1];
                    if (textures.find(path) == textures.end()) {
                        textures[path] = load_texture(ctx_, path);
                    }
                    current_material->spc_tex = textures[path];

                } else if (line_tokens[0].compare("map_d") == 0) {
                    // Currently unused
                } else if (line_tokens[0].compare("map_bump") == 0) {
                    // Currently unused
                }
            }
        }
    }

    static gl::texture2d load_texture(gl::context& ctx, std::string& filepath) {
        gl::texture2d tex = ctx.create_texture2d();

        std::vector<unsigned char> img;
        unsigned width, height;
        unsigned error = lodepng::decode(img, width, height, filepath);
        if (error) {
            throw std::runtime_error(
                std::string("Error loading texture ") +
                std::string(filepath) + std::string(".\nWith error :") +
                std::string(lodepng_error_text(error)));
        }

        tex.bind();
        tex.set_data(0, GL_RGBA, width, height, GL_RGBA,
                     GL_UNSIGNED_BYTE, &img[0]);
        tex.bind();
        tex.set_wrap_s(GL_REPEAT);
        tex.set_wrap_t(GL_REPEAT);
        tex.set_min_filter(GL_LINEAR);
        tex.set_mag_filter(GL_LINEAR);
        tex.unbind();
        return tex;
    }

    template<typename T>
    static T extract_data_from_index_token(std::string& index_token,
                                           std::vector<T>& data,
                                           const T& default_value) {
        using namespace std;
        T ret;
        long index;
        istringstream ss(index_token);
        ss >> index;
        if (index == 0) {
            ret = default_value;
        } else {
            if (index < 0) {
                ret = data[data.size() + index];
            } else {
                ret = data[index - 1];
            }
        }
        return ret;
    }

    static VERTEX parse_face(const std::string& face_token,
                             std::vector<glm::vec4>& positions,
                             std::vector<glm::vec3>& texcoords,
                             std::vector<glm::vec3>& normals,
                             parse_error& out_error) {
        using namespace std;
        using namespace glm;

        vector<string> face_indices = tokenize(face_token, "/");
        VERTEX ret;

        if (face_indices.size() >= 3) {
            if (face_indices.size() > 4) {
                out_error = TOO_MANY_FACE_INDICES;
            }
            ret.position = extract_data_from_index_token(face_indices[0],
                                                         positions,
                                                         vec4(0, 0, 0, 1));
            ret.texcoord = extract_data_from_index_token(face_indices[1],
                                                         texcoords,
                                                         vec3(0, 0, 1));
            ret.normal = extract_data_from_index_token(face_indices[2], normals,
                                                       vec3(0, 0, 0));
        } else if (face_indices.size() == 2) {
            ret.position = extract_data_from_index_token(face_indices[0],
                                                         positions,
                                                         vec4(0, 0, 0, 1));
            ret.texcoord = extract_data_from_index_token(face_indices[1],
                                                         texcoords,
                                                         vec3(0, 0, 1));
        } else if (face_indices.size() == 1) {
            ret.position = extract_data_from_index_token(face_indices[0],
                                                         positions,
                                                         vec4(0, 0, 0, 1));
        } else {
            out_error = MALFORMED_FACE;
            ret.position = vec4(0, 0, 0, 1);
            ret.texcoord = vec3(0, 0, 1);
            ret.normal = vec3(0, 0, 0);
        }

        return ret;
    }

    gl::context& ctx_;
    std::string base_dir_;
};
