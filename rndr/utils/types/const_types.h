// TODO: Kill glm precision hacks in header file
#define GLM_PRECISION_LOWP_UINT
#define GLM_PRECISION_LOWP_INT
#include <glm/glm.hpp>
#include <gl/gl.hpp>

#ifndef CONST_TYPES_H_
#define CONST_TYPES_H_

#define NULL_TYPE INT_MAX

#ifdef ANDROID
// TODO: This is bad...
#define GL_UNSIGNED_INT_VEC2 42
#define GL_UNSIGNED_INT_VEC3 43
#define GL_UNSIGNED_INT_VEC4 44
#endif

GLenum uniform_types[] = {
    GL_TEXTURE_2D,
    GL_BYTE,
    GL_UNSIGNED_BYTE,
    GL_SHORT,
    GL_UNSIGNED_SHORT,
    GL_INT,
    GL_UNSIGNED_INT,
    GL_FLOAT,
    GL_FLOAT_VEC2,
    GL_INT_VEC2,
    GL_UNSIGNED_INT_VEC2,
    GL_FLOAT_VEC3,
    GL_INT_VEC3,
    GL_UNSIGNED_INT_VEC3,
    GL_FLOAT_VEC4,
    GL_UNSIGNED_INT_VEC4,
    GL_FLOAT_MAT2,
    GL_FLOAT_MAT3,
    GL_FLOAT_MAT4
};

template <typename T>
struct uniform_type_info {
    enum {enum_type = NULL_TYPE};
};

template <typename T, size_t S>
struct uniform_type_info<T[S]> {
    enum {enum_type = uniform_type_info<T>::enum_type};
};

template <typename T>
struct uniform_type_info<T[]> {
    enum {enum_type = uniform_type_info<T>::enum_type};
};

template <>
struct uniform_type_info<gl::texture2d> {
    enum {enum_type = GL_TEXTURE_2D};
};

template <>
struct uniform_type_info<GLbyte> {
    enum {enum_type = GL_BYTE};
};
template <>
struct uniform_type_info<GLubyte> {
    enum {enum_type = GL_UNSIGNED_BYTE};
};
template <>
struct uniform_type_info<GLshort> {
    enum {enum_type = GL_SHORT};
};
template <>
struct uniform_type_info<GLushort> {
    enum {enum_type = GL_UNSIGNED_SHORT};
};
template <>
struct uniform_type_info<GLint> {
    enum {enum_type = GL_INT};
};
template <>
struct uniform_type_info<GLuint> {
    enum {enum_type = GL_UNSIGNED_INT};
};
template <>
struct uniform_type_info<GLfloat> {
    enum {enum_type = GL_FLOAT};
};
template <>
struct uniform_type_info<glm::vec2> {
    enum {enum_type = GL_FLOAT_VEC2};
};
template <>
struct uniform_type_info<glm::ivec2> {
    enum {enum_type = GL_INT_VEC2};
};
template <>
struct uniform_type_info<glm::uvec2> {
    enum {enum_type = GL_UNSIGNED_INT_VEC2};
};
template <>
struct uniform_type_info<glm::vec3> {
    enum {enum_type = GL_FLOAT_VEC3};
};
template <>
struct uniform_type_info<glm::ivec3> {
    enum {enum_type = GL_INT_VEC3};
};
template <>
struct uniform_type_info<glm::uvec3> {
    enum {enum_type = GL_UNSIGNED_INT_VEC3};
};
template <>
struct uniform_type_info<glm::vec4> {
    enum {enum_type = GL_FLOAT_VEC4};
};
template <>
struct uniform_type_info<glm::ivec4> {
    enum {enum_type = GL_INT_VEC4};
};
template <>
struct uniform_type_info<glm::uvec4> {
    enum {enum_type = GL_UNSIGNED_INT_VEC4};
};
template <>
struct uniform_type_info<glm::mat2> {
    enum {enum_type = GL_FLOAT_MAT2};
};
template <>
struct uniform_type_info<glm::mat3> {
    enum {enum_type = GL_FLOAT_MAT3};
};
template <>
struct uniform_type_info<glm::mat4> {
    enum {enum_type = GL_FLOAT_MAT4};
};

// GL_BYTE, GL_UNSIGNED_BYTE, GL_SHORT, GL_UNSIGNED_SHORT, GL_FIXED, or GL_FLOAT

template <GLenum TYPE>
struct attrib_type_info {
    enum {type = NULL_TYPE, size = -1};
};

template <>
struct attrib_type_info<GL_BYTE> {
    enum {type = GL_BYTE, size = 1};
};

template <>
struct attrib_type_info<GL_UNSIGNED_BYTE> {
    enum {type = GL_UNSIGNED_BYTE, size = 1};
};

template <>
struct attrib_type_info<GL_SHORT> {
    enum {type = GL_SHORT, size = 1};
};

template <>
struct attrib_type_info<GL_UNSIGNED_SHORT> {
    enum {type = GL_UNSIGNED_SHORT, size = 1};
};

template <>
struct attrib_type_info<GL_FIXED> {
    enum {type = GL_FIXED, size = 1};
};

template <>
struct attrib_type_info<GL_FLOAT> {
    enum {type = GL_FLOAT, size = 1};
};

template <>
struct attrib_type_info<GL_FLOAT_VEC2> {
    enum {type = GL_FLOAT, size = 2};
};

template <>
struct attrib_type_info<GL_INT_VEC2> {
#if(defined(GLM_PRECISION_LOWP_INT))
    enum {type = GL_SHORT, size = 2};
#else
#error "Can only use low precision vectors (short) in GLES"
#endif
};

template <>
struct attrib_type_info<GL_UNSIGNED_INT_VEC2> {
#if(defined(GLM_PRECISION_LOWP_UINT))
    enum {type = GL_UNSIGNED_SHORT, size = 2};
#else
#error "Can only use low precision vectors (unsigned short) in GLES"
#endif
};

template <>
struct attrib_type_info<GL_FLOAT_VEC3> {
    enum {type = GL_FLOAT, size = 3};
};

template <>
struct attrib_type_info<GL_INT_VEC3> {
#if(defined(GLM_PRECISION_LOWP_INT))
    enum {type = GL_SHORT, size = 3};
#else
#error "Can only use low precision vectors (short) in GLES"
#endif
};

template <>
struct attrib_type_info<GL_UNSIGNED_INT_VEC3> {
#if(defined(GLM_PRECISION_LOWP_UINT))
    enum {type = GL_UNSIGNED_SHORT, size = 3};
#else
#error "Can only use low precision vectors (unsigned short) in GLES"
#endif
};

template <>
struct attrib_type_info<GL_FLOAT_VEC4> {
    enum {type = GL_FLOAT, size = 4};
};

template <>
struct attrib_type_info<GL_INT_VEC4> {
#if(defined(GLM_PRECISION_LOWP_INT))
    enum {type = GL_SHORT, size = 4};
#else
#error "Can only use low precision vectors (short) in GLES"
#endif
};

template <>
struct attrib_type_info<GL_UNSIGNED_INT_VEC4> {
#if(defined(GLM_PRECISION_LOWP_UINT))
    enum {type = GL_UNSIGNED_SHORT, size = 4};
#else
#error "Can only use low precision vectors (unsigned short) in GLES"
#endif
};

#endif /* CONST_TYPES_H_ */
