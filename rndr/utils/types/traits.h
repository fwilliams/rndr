/*
 * traits.h
 *
 *  Created on: Jul 10, 2013
 *      Author: franciw
 */

#include <type_traits>

#ifndef TRAITS_H_
#define TRAITS_H_

struct reflection_base;

struct nulltype {
};

/*
 * Get information about an RTTI struct
 */
template<typename T, bool is_class>
struct rtti_struct_info {
    constexpr static const reflection_base* members_pointer();
};
template<typename T>
struct rtti_struct_info<T, true> {
    constexpr static const reflection_base* members_pointer() {
        return &T::info();
    }
};
template<typename T, size_t S>
struct rtti_struct_info<T[S], true> {
    constexpr static const reflection_base* members_pointer() {
        return &T::info();
    }
};
template<typename T>
struct rtti_struct_info<T, false> {
    constexpr static const reflection_base* members_pointer() {
        return 0;
    }
};

/*
 * Get information about a static array
 */
struct array_info {
    template<typename S, size_t N>
    constexpr static const size_t array_size(S (&a)[N]) {
        return N;
    }

    template<typename S>
    constexpr static const size_t array_size(S& a) {
        return 0;
    }
};

template<typename T, size_t S = 0>
struct is_struct {
    constexpr static bool val() {
        return std::is_class<T>::value;
    }
};
template<typename T, size_t S>
struct is_struct<T[S]> {
    constexpr static bool val() {
        return std::is_class<T>::value;
    }
};

/*
 * In this framework GLSL primitive types are treated as POD
 * types. The following specializations are needed to do this
 */
template<size_t S>
struct is_struct<glm::vec2[S]> {
    constexpr static bool val() {
        return false;
    }
};
template<size_t S>
struct is_struct<glm::vec3[S]> {
    constexpr static bool val() {
        return false;
    }
};
template<size_t S>
struct is_struct<glm::vec4[S]> {
    constexpr static bool val() {
        return false;
    }
};
template<size_t S>
struct is_struct<glm::mat2[S]> {
    constexpr static bool val() {
        return false;
    }
};
template<size_t S>
struct is_struct<glm::mat3[S]> {
    constexpr static bool val() {
        return false;
    }
};
template<size_t S>
struct is_struct<glm::mat4[S]> {
    constexpr static bool val() {
        return false;
    }
};
template<size_t S>
struct is_struct<gl::texture2d[S]> {
    constexpr static bool val() {
        return false;
    }
};

template<>
struct is_struct<glm::vec2> {
    constexpr static bool val() {
        return false;
    }
};
template<>
struct is_struct<glm::vec3> {
    constexpr static bool val() {
        return false;
    }
};
template<>
struct is_struct<glm::vec4> {
    constexpr static bool val() {
        return false;
    }
};
template<>
struct is_struct<glm::mat2> {
    constexpr static bool val() {
        return false;
    }
};
template<>
struct is_struct<glm::mat3> {
    constexpr static bool val() {
        return false;
    }
};
template<>
struct is_struct<glm::mat4> {
    constexpr static bool val() {
        return false;
    }
};
template<>
struct is_struct<gl::texture2d> {
    constexpr static bool val() {
        return false;
    }
};

#endif /* TRAITS_H_ */
