#include <typeinfo>
#include "../meta.h"

#ifndef REFLECT_H_
#define REFLECT_H_

struct type_base {
    virtual char const *name() const = 0;
    virtual size_t size() const = 0;
    virtual ~type_base() {
    }
};

template<typename T>
struct type: type_base {
    char const *name() const {
        return typeid(T).name();
    }
    size_t size() const {
        return sizeof(T);
    }
    virtual ~type() {
    }
};

// TODO: Make this a union and reduce it size!
struct member_t {
    char const *name;
    type_base *type;
    size_t offset;
    int gl_type_constant;	// Will be NO_GL_TYPE if not a gl type
    int enum_type;
    int num_elems;
    bool is_array;
    bool is_struct;
    size_t array_size;
    const reflection_base* refl_base;
};

struct reflection_base {
    void ReflectionConstruct();
    virtual ~reflection_base() {
    }
    virtual size_t size() const = 0;
    virtual char const *name() const = 0;
    virtual size_t member_count() const = 0;
    virtual member_t const *members() const = 0;
};

void reflection_base::ReflectionConstruct() {
    members();
    member_count();
    name();
    size();
}

template<typename T> T& instance() {
    static T t;
    return t;
}

template<typename T, typename Q>
type_base *get_type(Q T::*mem) {
    return &instance<type<Q> >();
}

#define __MEMBER__(x) \
   { #x, get_type(&T::x), (size_t)&((T*)0)->x, uniform_type_info<__typeof__(((T*)0)->x) >::enum_type,	\
	attrib_type_info<uniform_type_info<__typeof__(((T*)0)->x) >::enum_type >::type,						\
	attrib_type_info<uniform_type_info<__typeof__(((T*)0)->x) >::enum_type >::size,						\
    std::is_array<__typeof__(((T*)0)->x)>::value, is_struct<__typeof__(((T*)0)->x)>::val(),		 		\
    array_info::array_size((((T*)0)->x)), 																	\
	rtti_struct_info<__typeof__(((T*)0)->x), is_struct<__typeof__(((T*)0)->x)>::val()>::members_pointer()\
   },

#define __RTTI__(_type, _mems) 															    \
  template<typename T>                                                                      \
  struct _info : reflection_base {										                    \
    /* overrides used by reflection_base */													\
    inline size_t size() const { return sizeof(T); }										\
    inline char const *name() const { return #_type; }										\
    inline size_t member_count() const { size_t cnt; get_members(cnt); return cnt; }		\
    inline member_t const *members() const { size_t cnt; return get_members(cnt); }		    \
    _info() { ReflectionConstruct(); }														\
    static inline member_t const *get_members(size_t &cnt) {								\
      static member_t members[] = { _mems };												\
      cnt = sizeof(members) / sizeof(members[0]);											\
      return members;																		\
    }																						\
    static inline _info<T> &info() {														\
      return instance<_info<T> >();														    \
    }																						\
  };																						\
  inline static member_t const * members() { return _info<_type>::info().members(); }		\
  inline static _info<_type> &info() { static _info<_type> _theInfo; return _theInfo; }

#define PARAMS(type, N, ...) 	\
		__RTTI__(type, MAP(N, __MEMBER__, __VA_ARGS__) )

#endif /* REFLECT_H_*/
