/*
 * meta.h
 *
 *  Created on: Jun 4, 2013
 *      Author: franciw
 */

#ifndef META_H_
#define META_H_

#define DECLARE(type, name) \
	type name;

#define DECLARE_PRIVATE(type, name)	\
	type name##_;

#define MAP(N, FN, ...) \
	_MAP_##N(FN, __VA_ARGS__)

#define MAP2(N, FN, ...) \
	_MAP2_##N(FN, __VA_ARGS__)

#define COUNT_MAP2(N, FN, ...) \
	_CNT_MAP2_##N(FN, __VA_ARGS__)

#define _MAP_1(FN, a) \
	FN(a)
#define _MAP_2(FN, a, b) \
	_MAP_1(FN, a) FN(b)
#define _MAP_3(FN, a, b, c) \
	_MAP_2(FN, a, b) FN(c)
#define _MAP_4(FN, a, b, c, d) \
	_MAP_3(FN, a, b, c) FN(d)
#define _MAP_5(FN, a, b, c, d, e) \
	_MAP_4(FN, a, b, c, d) FN(e)
#define _MAP_6(FN, a, b, c, d, e, f) \
	_MAP_5(FN, a, b, c, d, e) FN(f)
#define _MAP_7(FN, a, b, c, d, e, f, g) \
	_MAP_6(FN, a, b, c, d, e, f) FN(g)
#define _MAP_8(FN, a, b, c, d, e, f, g, h) \
	_MAP_7(FN, a, b, c, d, e, f, g) FN(h)
#define _MAP_9(FN, a, b, c, d, e, f, g, h, i) \
	_MAP_8(FN, a, b, c, d, e, f, g, h) FN(i)
#define _MAP_10(FN, a, b, c, d, e, f, g, h, i, j) \
	_MAP_9(FN, a, b, c, d, e, f, g, h, i) FN(j)
#define _MAP_11(FN, a, b, c, d, e, f, g, h, i, j, k) \
	_MAP_10(FN, a, b, c, d, e, f, g, h, i, j) FN(k)
#define _MAP_12(FN, a, b, c, d, e, f, g, h, i, j, k, l) \
	_MAP_11(FN, a, b, c, d, e, f, g, h, i, j, k) FN(l)
#define _MAP_13(FN, a, b, c, d, e, f, g, h, i, j, k, l, m) \
	_MAP_12(FN, a, b, c, d, e, f, g, h, i, j, k, l) FN(m)
#define _MAP_14(FN, a, b, c, d, e, f, g, h, i, j, k, l, m, n) \
	_MAP_13(FN, a, b, c, d, e, f, g, h, i, j, k, l, m) FN(n)
#define _MAP_15(FN, a, b, c, d, e, f, g, h, i, j, k, l, m, n, o) \
	_MAP_14(FN, a, b, c, d, e, f, g, h, i, j, k, l, m, n) FN(o)
#define _MAP_16(FN, a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p) \
	_MAP_15(FN, a, b, c, d, e, f, g, h, i, j, k, l, m, n, o) FN(p)

#define _MAP2_1(FN, a, b) \
	FN(a, b)
#define _MAP2_2(FN, a, b, c, d) \
	_MAP2_1(FN, a, b) FN(c, d)
#define _MAP2_3(FN, a, b, c, d, e, f) \
	_MAP2_2(FN, a, b, c, d) FN(e, f)
#define _MAP2_4(FN, a, b, c, d, e, f, g, h) \
	_MAP2_3(FN, a, b, c, d, e, f) FN(g, h)
#define _MAP2_5(FN, a, b, c, d, e, f, g, h, i, j) \
	_MAP2_4(FN, a, b, c, d, e, f, g, h) FN(i, j)
#define _MAP2_6(FN, a, b, c, d, e, f, g, h, i, j, k, l) \
	_MAP2_5(FN, a, b, c, d, e, f, g, h, i, j) FN(k, l)
#define _MAP2_7(FN, a, b, c, d, e, f, g, h, i, j, k, l, m, n) \
	_MAP2_6(FN, a, b, c, d, e, f, g, h, i, j, k, l) FN(m, n)
#define _MAP2_8(FN, a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p) \
	_MAP2_7(FN, a, b, c, d, e, f, g, h, i, j, k, l, m, n) FN(o, p)
#define _MAP2_9(FN, a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r) \
	_MAP2_8(FN, a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p) FN(q, r)
#define _MAP2_10(FN, a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t) \
	_MAP2_9(FN, a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r) FN(s, t)
#define _MAP2_11(FN, a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t, u, v) \
	_MAP2_10(FN, a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t) FN(u, v)
#define _MAP2_12(FN, a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t, u, v, w, x) \
	_MAP2_11(FN, a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t, u, v) FN(w, x)
#define _MAP2_13(FN, a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t, u, v, w, x, y, z) \
	_MAP2_12(FN, a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t, u, v, w, x) FN(y, z)
#define _MAP2_14(FN, a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t, u, v, w, x, y, z, a0, b0) \
	_MAP2_13(FN, a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t, u, v, w, x, y, z) FN(a0, b0)
#define _MAP2_15(FN, a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t, u, v, w, x, y, z, a0, b0, c0, d0) \
	_MAP2_14(FN, a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t, u, v, w, x, y, z, a0, b0) FN(c0, d0)
#define _MAP2_16(FN, a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t, u, v, w, x, y, z, a0, b0, c0, d0, e0, f0) \
	_MAP2_15(FN, a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t, u, v, w, x, y, z, a0, b0, c0, d0) FN(e0, f0)

#define _CNT_MAP2_1(FN, a, b) \
	FN(0, a, b)
#define _CNT_MAP2_2(FN, a, b, c, d) \
	_CNT_MAP2_1(FN, a, b) FN(1, c, d)
#define _CNT_MAP2_3(FN, a, b, c, d, e, f) \
	_CNT_MAP2_2(FN, a, b, c, d) FN(2, e, f)
#define _CNT_MAP2_4(FN, a, b, c, d, e, f, g, h) \
	_CNT_MAP2_3(FN, a, b, c, d, e, f) FN(3, g, h)
#define _CNT_MAP2_5(FN, a, b, c, d, e, f, g, h, i, j) \
	_CNT_MAP2_4(FN, a, b, c, d, e, f, g, h) FN(4, i, j)
#define _CNT_MAP2_6(FN, a, b, c, d, e, f, g, h, i, j, k, l) \
	_CNT_MAP2_5(FN, a, b, c, d, e, f, g, h, i, j) FN(5, k, l)
#define _CNT_MAP2_7(FN, a, b, c, d, e, f, g, h, i, j, k, l, m, n) \
	_CNT_MAP2_6(FN, a, b, c, d, e, f, g, h, i, j, k, l) FN(6, m, n)
#define _CNT_MAP2_8(FN, a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p) \
	_CNT_MAP2_7(FN, a, b, c, d, e, f, g, h, i, j, k, l, m, n) FN(7, o, p)
#define _CNT_MAP2_9(FN, a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r) \
	_CNT_MAP2_8(FN, a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p) FN(8, q, r)
#define _CNT_MAP2_10(FN, a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t) \
	_CNT_MAP2_9(FN, a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r) FN(9, s, t)
#define _CNT_MAP2_11(FN, a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t, u, v) \
	_CNT_MAP2_10(FN, a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t) FN(10, u, v)
#define _CNT_MAP2_12(FN, a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t, u, v, w, x) \
	_CNT_MAP2_11(FN, a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t, u, v) FN(11, w, x)
#define _CNT_MAP2_13(FN, a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t, u, v, w, x, y, z) \
	_CNT_MAP2_12(FN, a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t, u, v, w, x) FN(12, y, z)
#define _CNT_MAP2_14(FN, a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t, u, v, w, x, y, z, a0, b0) \
	_CNT_MAP2_13(FN, a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t, u, v, w, x, y, z) FN(13, a0, b0)
#define _CNT_MAP2_15(FN, a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t, u, v, w, x, y, z, a0, b0, c0, d0) \
	_CNT_MAP2_14(FN, a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t, u, v, w, x, y, z, a0, b0) FN(14, c0, d0)
#define _CNT_MAP2_16(FN, a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t, u, v, w, x, y, z, a0, b0, c0, d0, e0, f0) \
	_CNT_MAP2_15(FN, a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t, u, v, w, x, y, z, a0, b0, c0, d0) FN(15, e0, f0)

#endif /* META_H_ */
