/*
 * test.cpp
 *
 *  Created on: Jun 17, 2014
 *      Author: francis
 */

#include <iostream>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <sdl_viewer/sdl_viewer.hpp>
#include "geometry.hpp"

typedef vertex_position_normal vertex;

/*
 * program p(
 * 		{{"file1.shader", gl::vert_shader},
 * 		 {"file2.shader", gl::frag_shader}.
 * 		 {"file3.shader", gl::geom_shader},
 * 		 {"file4.shader", gl::tess_shader},
 * 		 {"file5.shader", gl::comp_shader}});
 */

struct test_scene {
    gl::program shader;
    gl::vbo<vertex> geometry;
    gl::ibo<glm::uint> indices;
    flat_color_shader_params params;
    glm::mat4 model;

    const float angleperframe = 0.5f;

    void init(gl::context& ctx) {
        ctx.set_clear_color(0.02f, 0.02f, 0.2f, 1.0f);
        ctx.enable_depth_test();

        shader = ctx.create_program_from_files(
                "assets/shaders/position_normal.vert",
                "assets/shaders/blinn_phong_directional.frag");

        geometry = ctx.create_vbo<vertex>();
        indices = ctx.create_ibo<glm::uint>();

        params.projection_mat = glm::frustum(-0.5f, 0.5f, -0.5f, 0.5f, 0.5f,
                                             1000.0f);

        params.diffuse_color = glm::vec4(0.2, 0.2, 0.2, 1.0);
        params.specular_color = glm::vec4(0.5, 0.5, 1.0, 1.0);
        params.shine_constant = 10.0f;
        params.light_direction = glm::vec3(0.0, 1.0, 1.0);

        shader.set_uniforms_from_struct(params);

        tetrahedron<vertex> t;

        geometry.set_data(t.num_vertices, t.vertices.data(), GL_STATIC_DRAW);
        indices.set_data(t.num_indices, t.indices.data(), GL_STATIC_DRAW);
    }

    void draw(gl::context& ctx) {
        ctx.clear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        const glm::mat4 view = glm::lookAt(glm::vec3(3.0, 3.0, 0.0), glm::vec3(0.0, 0.0, 0.0), glm::vec3(0.0, 1.0, 0.0));
        model = glm::rotate(model, angleperframe,
                                glm::normalize(glm::vec3(0.0, 1.0, 0.0)));

        params.modelview_mat = view * model;
        params.normal_mat = glm::transpose(glm::inverse(glm::mat3(params.modelview_mat)));

        shader.set_uniforms_from_struct(params);

        ctx.bind_ibo(indices);
        ctx.bind_vbo(geometry);
        ctx.bind_program(shader);

        geometry.setup_attrib_array(shader.get_attributes<vertex>());

        glDrawElements(GL_TRIANGLES, tetrahedron<vertex>::num_indices,
                       GL_UNSIGNED_INT, nullptr);

        ctx.unbind_vbo();
        ctx.unbind_ibo();
        ctx.unbind_program();
    }

    void handle_event(gl::context& ctx, const SDL_Event& event) {
    }

    void destroy(gl::context& ctx) {
    }
};

sdl_viewer_main(test_scene, 30, "OpenGL framework test", 800, 600);
