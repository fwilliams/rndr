/*
 * geometry.hpp
 *
 *  Created on: Jun 17, 2014
 *      Author: francis
 */

#ifndef GEOMETRY_HPP_
#define GEOMETRY_HPP_

template<typename vertex>
struct octohedron {
    octohedron() {
        using namespace glm;

        vertices.reserve(num_vertices * sizeof(vertex));
        vertices[0].position = vec4(1.0, 0.0, 0.0, 1.0);
        vertices[1].position = vec4(-1.0, 0.0, -0.0, 1.0);
        vertices[2].position = vec4(0.0, 1.0, -0.0, 1.0);
        vertices[3].position = vec4(0.0, -1.0, 0.0, 1.0);
        vertices[4].position = vec4(0.0, 0.0, 1.0, 1.0);
        vertices[5].position = vec4(0.0, 0.0, -1.0, 1.0);

        indices.reserve(num_indices * sizeof(unsigned));
        indices[0] = 0;
        indices[1] = 2;
        indices[2] = 4;
        indices[3] = 2;
        indices[4] = 0;
        indices[5] = 5;
        indices[6] = 3;
        indices[7] = 0;
        indices[8] = 4;
        indices[9] = 0;
        indices[10] = 3;
        indices[11] = 5;
        indices[12] = 2;
        indices[13] = 1;
        indices[14] = 4;
        indices[15] = 1;
        indices[16] = 2;
        indices[17] = 5;
        indices[18] = 1;
        indices[19] = 3;
        indices[20] = 4;
        indices[21] = 3;
        indices[22] = 1;
        indices[23] = 5;

        for (unsigned i = 0; i < num_triangles; i++) {
            vec3 v[3];
            for (unsigned j = 0; j < 3; j++) {
                v[j] = vec3(vertices[indices[i * 3 + j]].position);
            }

            static const float degree_per_vertex = 4.0f;
            vec3 normal = normalize(cross(v[0] - v[2], v[1] - v[2]))
                    / degree_per_vertex;

            for (unsigned j = 0; j < 3; j++) {
                vertices[indices[i * 3 + j]].normal += normal;
            }
        }
    }

    static const int num_vertices = 6;
    static const int num_indices = 24;
    static const int num_triangles = 8;

    std::vector<vertex> vertices;
    std::vector<glm::uint> indices;
};

template<typename vertex>
struct tetrahedron {
    tetrahedron() {
        using namespace glm;

        vertices.reserve(num_vertices * sizeof(vertex));
        vertices[0].position = vec4(1.0, 1.0, 1.0, 1.0);
        vertices[1].position = vec4(1.0, -1.0, -1.0, 1.0);
        vertices[2].position = vec4(-1.0, 1.0, -1.0, 1.0);
        vertices[3].position = vec4(-1.0, -1.0, 1.0, 1.0);

        indices.reserve(num_indices * sizeof(unsigned));
        indices[0] = 3;
        indices[1] = 2;
        indices[2] = 1;
        indices[3] = 2;
        indices[4] = 3;
        indices[5] = 0;
        indices[6] = 1;
        indices[7] = 0;
        indices[8] = 3;
        indices[9] = 0;
        indices[10] = 1;
        indices[11] = 2;

        for (unsigned i = 0; i < num_triangles; i++) {
            vec3 v[3];
            for (unsigned j = 0; j < 3; j++) {
                v[j] = vec3(vertices[indices[i * 3 + j]].position);
            }

            static const float degree_per_vertex = 4.0f;
            vec3 normal = normalize(cross(v[0] - v[2], v[1] - v[2]))
                    / degree_per_vertex;

            for (unsigned j = 0; j < 3; j++) {
                vertices[indices[i * 3 + j]].normal += normal;
            }
        }
    }

    static const int num_vertices = 4;
    static const int num_indices = 12;
    static const int num_triangles = 4;

    std::vector<vertex> vertices;
    std::vector<glm::uint> indices;
};

struct vertex_position_normal {
    glm::vec4 position;
    glm::vec3 normal;

    PARAMS(vertex_position_normal, 2, position, normal)
};

struct flat_color_shader_params {
    glm::vec4 diffuse_color;
    glm::vec4 specular_color;
    glm::float_t shine_constant;
    glm::mat4 modelview_mat;
    glm::mat4 projection_mat;
    glm::mat3 normal_mat;
    glm::vec3 light_direction;

    PARAMS(flat_color_shader_params, 7, diffuse_color, specular_color, shine_constant, modelview_mat, projection_mat, normal_mat, light_direction)
};

#endif /* GEOMETRY_HPP_ */
