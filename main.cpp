#include <iostream>

#define GLM_PRECISION_LOWP_INT
#define GLM_PRECISION_MEDIUMP_FLOAT

#include <string>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
//#include <sdl_viewer/sdl_viewer.hpp>

#include <utils/loaders/obj_loader.h>

using namespace glm;
using namespace std;
using namespace gl;

#define NUM_LIGHTS 6

#ifdef ANDROID
#define BASE_DIR std::string("/sdcard/")
#else
#define BASE_DIR std::string("")
#endif

struct vertex {
    vec4 position;
    vec3 normal;
    vec3 texcoord;

    PARAMS(vertex, 3, position, normal, texcoord)};

struct textured_material {
    glm::vec4 ambient;
    glm::vec4 diffuse;
    glm::vec4 specular;
    float shine_exp;
    gl::texture2d amb_tex;
    gl::texture2d dif_tex;
    gl::texture2d spc_tex;

            PARAMS(textured_material, 7, ambient, diffuse, specular, shine_exp, amb_tex, dif_tex, spc_tex)};

struct camera {
    mat4 view_matrix;
    mat4 projection_matrix;

    PARAMS(camera, 2, view_matrix, projection_matrix)};

struct global_transforms {
    mat4 model_matrix;
    mat3 normal_matrix;

    PARAMS(global_transforms, 2, model_matrix, normal_matrix)};

struct global_lighting {
    struct light {
        vec4 ambient;
        vec4 position;
        vec4 intensity;
        float attenuation_factor;

        PARAMS(light, 3, position, intensity, attenuation_factor)};

        light lights[NUM_LIGHTS];

        PARAMS(global_lighting, 1, lights)
    };

struct scene {
    ivec2 viewport_dims;
    program prog;
    global_lighting light_params;
    global_transforms transforms;
    camera cam;

    obj_model<vertex, textured_material>* model;

    // radius, height, angle
    vec3 camparams = vec3(90.0, 1.0, 0.0);
    vec3 dcamparams = vec3(1.1, 1.1, 0.05);

    void init(gl::context& ctx) {
        ctx.set_clear_color(0.1, 0.1, 0.1, 1.0);
        ctx.enable_depth_test();

        prog = ctx.create_program_from_files(
        BASE_DIR+ std::string("assets/shaders/textured_lit.vert"),
        BASE_DIR + std::string("assets/shaders/textured_lit.frag"));

        light_params.lights[0].position = vec4(0.0, 100.0, 0.0, 1.0);
        light_params.lights[0].intensity = vec4(0.7, 0.7, 0.7, 1.0);
        light_params.lights[0].attenuation_factor = 100.0;

        light_params.lights[1].position = vec4(40.0, 80.0, 0.0, 1.0);
        light_params.lights[1].intensity = vec4(0.7, 0.7, 0.7, 1.0);
        light_params.lights[1].attenuation_factor = 100.0;

        light_params.lights[2].position = vec4(-40.0, 80.0, 0.0, 1.0);
        light_params.lights[2].intensity = vec4(0.7, 0.7, 0.7, 1.0);
        light_params.lights[2].attenuation_factor = 100.0;

        light_params.lights[3].position = vec4(0.0, 180.0, 40.0, 1.0);
        light_params.lights[3].intensity = vec4(0.7, 0.7, 0.7, 1.0);
        light_params.lights[3].attenuation_factor = 100.0;

        light_params.lights[4].position = vec4(0.0, 80.0, -40.0, 1.0);
        light_params.lights[4].intensity = vec4(0.7, 0.7, 0.7, 1.0);
        light_params.lights[4].attenuation_factor = 100.0;

        transforms.model_matrix = scale(mat4(1.0), vec3(1.0, -1.0, 1.0));
        transforms.normal_matrix = transpose(
                inverse(mat3(transforms.model_matrix)));
        cam.projection_matrix = frustum(-0.5, 0.5, -0.5, 0.5, 0.5, 1000.0);

        vec3 pos = vec3(camparams[0] * cos(camparams[2]), camparams[1],
                        camparams[0] * sin(camparams[2]));
        cam.view_matrix = lookAt(pos, vec3(0.0), vec3(0.0, 1.0, 0.0));

        prog.bind();

        prog.set_uniforms_from_struct(light_params);
        prog.set_uniforms_from_struct(transforms);
        prog.set_uniforms_from_struct(cam);

        model = new obj_model<vertex, textured_material>(ctx,
                                                         "models/bottle.obj",
                                                         BASE_DIR+ std::string("assets/"));
    }

    void handle_event(gl::context& ctx, const SDL_Event& event) {}

    void draw(gl::context& ctx) {
        transforms.model_matrix *= rotate(mat4(1.0), 1.0f, vec3(0.0, 1.0, 0.0));
        prog.set_uniforms_from_struct(transforms);

        ctx.clear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        for(int i = 0; i < model->geometry.size(); ++i) {
            prog.set_uniforms_from_struct(*(model->mats[i]));
            model->geometry[i].bind();
            model->geometry[i].setup_attrib_array(prog.get_attributes<vertex>());
            glDrawArrays(GL_TRIANGLES, 0, model->geometry[i].get_element_count());
        }
    }

    void destroy(gl::context& ctx) {
        delete model;
    }
};

sdl_viewer_main(scene, 30, "SDL/OpenGL Test", 640, 480)
