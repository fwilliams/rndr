$version 440

#define MAX_LIGHTS 16
#define PI 3.1415926535897932384626433832795

struct light {
	vec3 position;
	vec4 intensity;
	// vec4 attenuation;
};

uniform light[MAX_LIGHTS] lights;

uniform vec4 diffuse;
uniform float recractive_index;
uniform float shine_factor;

in vec3 vnormal;
in vec4 vposition;

out vec4 fragcolor;

void main() {
	vec4 viewdir = normalize(-vposition);
	vec4 dirtolight;
	float lightdist;
	
	for(uint i = 0; i < MAX_LIGHTS; ++i) {
		// compute diffuse term
		fragcolor += 
			diffuse * 
			vec4(dot(vnormal, dirtolight), 1.0) * 
			light[i].intensity;
		
		// compute specular term
		float fresnel = 
			pow((1 - recractive_index) / (1 + recractive_index), 2.0);
		
		vec4 half_vec = viewdir + dirtolight;
		halv_vec = half_vec / normalize(half_vec);
		
		vec4 dist_term = 
			((shine_factor + 2.0)/(2.0 * PI)) * 
			pow(half_vec, shine_factor);
			
		vec4 geom_term = 
			min(
				1.0, 
				2.0 * (half_vec)*(
	}
}