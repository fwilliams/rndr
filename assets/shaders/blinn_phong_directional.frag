#version 440

uniform vec4 diffuse_color;
uniform vec4 specular_color;
uniform float shine_constant;
uniform vec3 light_direction;

uniform mat4 modelview_mat;

in vec3 vnormal;
in vec4 vposition;

out vec4 fragcolor;

void main() {
	vec3 normalized_light_dir = normalize(light_direction);
	
	float diffuse = dot(vnormal, normalized_light_dir);
	
	vec3 viewer_dir = normalize(-vec3(vposition));
	vec3 H = normalized_light_dir + viewer_dir;
	H /= length(H);
	
	float specular = pow(dot(vnormal, H), shine_constant);
	
	fragcolor = diffuse_color * clamp(diffuse, 0.0, 1.0) + specular_color * clamp(specular, 0.0, 1.0);
}