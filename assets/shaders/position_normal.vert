#version 440

uniform mat4 modelview_mat;
uniform mat4 projection_mat;
uniform mat3 normal_mat;

in vec4 position;
in vec3 normal;

out vec3 vnormal;
out vec4 vposition;

void main() {
	gl_Position = projection_mat * modelview_mat * position;
	vposition = modelview_mat * position;
	vnormal = normalize(normal_mat * normal);
}